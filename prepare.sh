#!/bin/bash
set -e

OLM_VERSION="3.2.13" # https://gitlab.matrix.org/matrix-org/olm/-/releases
CMAKE_VERSION="3.24.2" # https://cmake.org/download/
NDK_VERSION="r25b" # https://developer.android.com/ndk/downloads/

case "$(uname -s)" in
    Linux*)     export HOST=Linux;;
    Darwin*)    export HOST=Mac;;
    *)          export HOST="UNKNOWN"
esac
echo "Detected host is ${HOST}"

echo "Download and unzip olm"
wget "https://gitlab.matrix.org/matrix-org/olm/-/archive/$OLM_VERSION/olm-$OLM_VERSION.zip" -O olm.zip
unzip olm.zip
rm olm.zip
mv olm-$OLM_VERSION olm

echo "Download and install cmake"
if [ $HOST == "Linux" ];
then
  CMAKE_TAR=cmake-$CMAKE_VERSION-linux-x86_64.tar.gz
else
  CMAKE_TAR=cmake-$CMAKE_VERSION-macos-universal.tar.gz
fi
wget "https://github.com/Kitware/CMake/releases/download/v$CMAKE_VERSION/$CMAKE_TAR" -O cmake.tar.gz
tar -xf cmake.tar.gz
rm cmake.tar.gz
if [ $HOST == "Linux" ];
then
  mv cmake-$CMAKE_VERSION-linux-x86_64 cmake
else
  mv cmake-$CMAKE_VERSION-macos-universal/CMake.app/Contents cmake
fi
export PATH="cmake/bin:$PATH"

if [ $HOST == "Linux" ];
then
  wget "https://dl.google.com/android/repository/android-ndk-$NDK_VERSION-linux.zip" -O ndk.zip
  unzip ndk.zip
  rm ndk.zip
  mv android-ndk-$NDK_VERSION ndk
fi

if [ $HOST == "Mac" ];
then
  wget "https://raw.githubusercontent.com/leetal/ios-cmake/master/ios.toolchain.cmake" -O olm/ios.toolchain.cmake
fi

echo "finished"