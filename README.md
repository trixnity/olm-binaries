# olm-binaries

This project builds olm binaries for [Trixnity](https://gitlab.com/trixnity/trixnity).

The directories of the shared binaries should be named as needed by JNA.

The directories of the static binaries should be named as needed by Konan (Kotlin Native).
