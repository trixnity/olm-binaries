#!/bin/bash
set -e

mkdir build

###############################################
# For Trixnity JVM target.
###############################################
echo "build shared libs"
SHARED_DIR="build/shared"
buildSharedDesktop() {
  echo "... for target: $1"
  cmake olm -B$SHARED_DIR/"$1" -DOLM_TESTS=OFF "${@:2}"
  cmake --build $SHARED_DIR/"$1"
}
buildSharedDesktopMingw(){
  echo "... for target: $1"
    x86_64-w64-mingw32.static-cmake olm_mingw -B$SHARED_DIR/"$1" -DOLM_TESTS=OFF "${@:2}"
    x86_64-w64-mingw32.static-cmake --build $SHARED_DIR/"$1"
    x86_64-w64-mingw32.static-strip $SHARED_DIR/"$1"/libolm.dll
    mv $SHARED_DIR/"$1"/libolm.dll $SHARED_DIR/"$1"/olm.dll
}
if [ "$HOST" == "Linux" ];
then
  buildSharedDesktop linux-x86-64
  cp -r olm/ olm_mingw/
  sed -i 's/\(library(olm\)\b/\1 SHARED/' olm_mingw/CMakeLists.txt
  buildSharedDesktopMingw win32-x86-64
fi
if [ "$HOST" == "Mac" ];
then
  buildSharedDesktop darwin -DCMAKE_OSX_ARCHITECTURES=x86_64\;arm64
fi

###############################################
# For Trixnity Android target.
###############################################
echo "build shared android libs"
if [ "$HOST" == "Linux" ];
then
  SHARED_ANDROID_DIR="build/shared-android"
  buildSharedAndroid() {
    echo "... for target: $1"
    cmake olm -B$SHARED_ANDROID_DIR/"$1" -DOLM_TESTS=OFF -DCMAKE_TOOLCHAIN_FILE=../ndk/build/cmake/android.toolchain.cmake "${@:2}"
    cmake --build $SHARED_ANDROID_DIR/"$1"
  }
  buildSharedAndroid armeabi-v7a -DANDROID_ABI=armeabi-v7a
  buildSharedAndroid arm64-v8a -DANDROID_ABI=arm64-v8a
  buildSharedAndroid x86 -DANDROID_ABI=x86
  buildSharedAndroid x86_64 -DANDROID_ABI=x86_64
fi

###############################################
# For Trixnity native targets.
###############################################
echo "build static libs"
STATIC_DIR="build/static"
buildStatic() {
  echo "... for target: $1"
  cmake olm -B$STATIC_DIR/"$1" -DOLM_TESTS=OFF -DBUILD_SHARED_LIBS=NO "${@:2}"
  cmake --build $STATIC_DIR/"$1"
}
if [ "$HOST" == "Linux" ];
then
  buildStatic linux_x64
  buildStatic mingw_x64 -DCMAKE_TOOLCHAIN_FILE=Windows64.cmake
fi
if [ "$HOST" == "Mac" ];
then
  buildStatic macos_arm64 -DCMAKE_OSX_ARCHITECTURES=arm64
  buildStatic macos_x64 -DCMAKE_OSX_ARCHITECTURES=x86_64
  buildStatic ios_simulator_arm64 ""-DCMAKE_TOOLCHAIN_FILE=ios.toolchain.cmake -DPLATFORM=SIMULATORARM64
  buildStatic ios_arm64 -DCMAKE_TOOLCHAIN_FILE=ios.toolchain.cmake -DPLATFORM=OS64
  buildStatic ios_x64 -DCMAKE_TOOLCHAIN_FILE=ios.toolchain.cmake -DPLATFORM=SIMULATOR64
fi

mkdir binaries
(cd build; find . \( -name "libolm.so" -or -name "libolm.a" -or -name "olm.dll" -or -name "libolm.dylib" \) -exec rsync -R --copy-links '{}' ../binaries \;)

echo "finished"
